Scope of our services
=====================

What we provide
---------------

Debian.net exists to provide infrastructure for Debian Developers to
host services in support of Debian.

Currently we provide un-managed virtual machines to Debian Developers.

In the future, we plan to provide semi-managed hosting, with
configuration management, centralized backups, monitoring, etc.

Who can use the service
-----------------------

Any Debian Developer can request hosting from debian.net for a project.
See :ref:`eligibility`.

Debian.net compared to DSA
--------------------------

The `Debian System Administrator (DSA)`_ team is responsible for
hosting Debian's core infrastructure.

However, they do not have the capacity to provide hosting for all the
ancillary infrastructure around Debian development.
In the past, the only options Debian Developers had were: DSA hosting,
using their own personal machines, or finding sponsored hosting
themselves.
The Debian.net team aims to provide centralised coordination for
hosting, for Developers that wish to use it.

.. _Debian System Administrator (DSA): https://dsa.debian.org/

If your service is core to Debian development, we suggest you discuss
hosting it with DSA, first.
We'd welcome the migration of any service from Debian.net to DSA
infrastructure, or vice-versa.

The debian.net domain
---------------------

While most services hosted by this team are served as subdomains on the
`debian.net domain`_, that domain is not exclusive to this team.

Any Debian Developer can host any Debian service they wish, wherever
they want, and serve it through the `debian.net domain`_.

.. _debian.net domain: https://wiki.debian.org/DebianDotNet
