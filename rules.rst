.. _rules:

Rules
=====

Machine Usage Policy
--------------------

Debian.net Team hosting follows the standard `Debian Machine Usage
Policies`_, with the following addition, when referring to Debian.net
infrastructure:

 * The term DSA is extended to include the The Debian.net Team, as well
   as the Debian Systems Administration Team.

.. _Debian Machine Usage Policies: https://www.debian.org/devel/dmup

.. _eligibility:

Eligibility
-----------

All Debian Developers are eligible to request services from the
Debian.net Team. Project contributors who are not yet Debian Developers
can make requests if sponsored by a Debian Developer, who will take
joint responsibility for the requested infrastructure.

.. _responsibilities:

Responsibilities
----------------

Un-managed VMs
~~~~~~~~~~~~~~

You'll be fully responsible for your instances.

The Debian.net Team will have root on your instance, but will generally
not interfere with it, unless you request help.

If there is a security or abuse incident, the machine may be suspended
until you can investigate it, or terminated.

We recommend that you:

#. Find some other co-maintainers for your service.
#. Lock down ssh access, disabling password authentication, and only
   using public keys.
#. Schedule automated backups, e.g. to rsync.net (who `provide 500GB of
   backup space to Debian Developers <https://www.rsync.net/debian.html>`_).
#. Install and enable ``unattended-upgrades``.
#. (optionally) lurk in the ``#debian.net`` IRC channel.
