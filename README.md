How to update this documentation
--------------------------------

This documentation uses Sphinx. You will need to install it if you want to
regenerate the HTML files:

  $ apt install python3-sphinx

To modify the documentation, modify the ReStructuredText files (.rst). Once that
is done, run this command to generate the HTML files:

  $ make html

The HTML output directory is `_build/html`.

If you want a new page to be added to the sidebar menu, add it to the
`index.rst` file.

License
-------

This documentation is licensed under the Creative Commons
Attribution-ShareAlike 4.0 International license. See LICENSE.
