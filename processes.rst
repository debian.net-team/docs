Processes
=========

.. _file a ticket: https://salsa.debian.org/debian.net-team/requests/-/issues/new
.. _requesting_infrastructure:

Requesting Infrastructure
-------------------------

#. `File a ticket`_ in the requests repo, using the
   ``create_vps_instance`` template.
#. Pick a hostname for your instance, that's available on the
   ``debian.net`` domain (or elsewhere).
#. Tell us what you're going to need, what it's going to be used for,
   and who will be looking after it.
#. Review the :ref:`rules` and your :ref:`responsibilities`.

Reporting Abuse
---------------

#. Please `File a ticket`_ in the requests repo, using the ``abuse``
   template. Or contact the Debian.net admins directly.

Salvaging Infrastructure
------------------------

Is there are service maintained on Debian.net infrastructure that's
appears to be orphaned, or the maintainer is MIA?

1. Attempt to contact the current maintainers of the service. These are
   documented in the `infrastructure repo`_.
2. `File a ticket`_ in the requests repo, using the ``salvage``
   template.

.. _infrastructure repo: https://salsa.debian.org/debian.net-team/infra

Retiring Infrastructure
-----------------------

Don't need your instance any more?

1. `File a ticket`_ in the requests repo, using the ``retirement``
   template.
