Providers
=========

We currently host through commercial providers, and the Debian Project
covers the hosting charges.
We aim to take advantage of sponsored hardware and infrastructure
credits, where possible, but these are not in place, yet.

We're working with a large hosting provider to set up an OpenStack
instance as well and we hope to be in a position to host instances there
before August 2021.
We'll share some more news there as soon as we can.

We currently host instances at:

 * Hetzner Cloud: `Pricing <https://www.hetzner.com/cloud>`__.
 * Digital Ocean: `Pricing <https://www.digitalocean.com/pricing/calculator/>`__.

Budget
------

The Debian.Net team has a budget from the DPL (currently 250/month), to
cover small instances at the team's discretion.
Larger requests go to the DPL for approval.
